<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Http\Request;
use Validator;

class ProjectController extends Controller
{
    public function new(Request $request, $user_id)
    {

        $randomKey = substr(str_replace(['+', '/', '=', '@', '-', '_'], '', base64_encode(random_bytes(9))), 0, 9);
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $total = Project::where('user_id', $user_id)->count();

        if ($total >= 2) {
            return response()->json([
                'message' => "Sorry, this is the limit for you",
                'data' => []
            ], 200);
        }

        $project = Project::create(array_merge($validator->validated(), [
            'user_id' => $user_id,
            'key' => $randomKey
        ]));


        return response()->json([
            'message' => "Alhamdulillah, new project added",
            'data' => $project
        ], 200);
    }

    public function all($user_id)
    {

        $projects = Project::where('user_id', $user_id)->cursor();
        $result = [];

        foreach ($projects as $project) {
            $result[] = $project;
        }

        return response()->json([
            'message' => "This is your projects",
            'data' => $result,
        ], 200);

    }
}
