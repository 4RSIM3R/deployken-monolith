<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\ProjectController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', [AuthController::class, 'login']);
Route::post('/register', [AuthController::class, 'register']);

Route::group([
    'middleware' => 'auth:api',
    'prefix' => 'projects'
], function ($router) {

    Route::get('/tes', function(){
        return "Tes";
    });
    
    Route::post('/new/{user_id}', [ProjectController::class, 'new']);
    Route::get('/all/{user_id}', [ProjectController::class, 'all']);
    // Route::delete('/delete/{user_id}/{project_id}', [ProjectController::class, 'delete']);
    // Route::put('/update/{user_id}/{project_id}', [ProjectController::class, 'update']);

});
